package com.katrina.katrina_fil_rouge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KatrinaFilRougeApplication {

	public static void main(String[] args) {
		SpringApplication.run(KatrinaFilRougeApplication.class, args);
	}

}
